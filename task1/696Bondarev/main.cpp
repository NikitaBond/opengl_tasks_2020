#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>


#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <stdlib.h>
#include <algorithm>
#include <SOIL2.h>

class MapLoader {
public:
    std::vector<std::vector<float>> height_map;
    int width;
    int height;
    int channels;
    int max;

    MapLoader()
        :max(0)
    {}

    void ReadMap() {
        unsigned char* ht_map = SOIL_load_image
                (
                        "592LitvintsevaData1/australia.png",
                        &width, &height, &channels,
                        SOIL_LOAD_L
                );

        if (ht_map == NULL) {
            std::cout << "NULL map, check file paths" << std::endl;
            std::__throw_runtime_error("error");
        }

        assert(channels == 1);

        for (int i = 0; i < height; ++i) {
            height_map.push_back(std::vector<float>(width));
            for (int j = 0; j < width; ++j) {
                height_map[i][j] = static_cast<float>(ht_map[j]);

                if (max < height_map[i][j]) {
                    max = height_map[i][j];
                }
            }
            ht_map += width;
        }

        normalize();
    }

private:
    void normalize() {
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                height_map[i][j] /= max;
            }
        }
    }
};

class SampleApplication : public Application
{
public:
    MapLoader map;
    MeshPtr _surface;
    ShaderProgramPtr _shader;
    std::vector<glm::vec3> _points;
    std::vector<glm::vec3> _normals;


    SampleApplication() {
        const int k_neighbours = 6;

        uint8_t neighbours[k_neighbours][2] = {
                {0, 0},
                {0, 1},
                {1, 0},
                {0, 1},
                {1, 0},
                {1, 1},
        };

        map.ReadMap();
        std::cerr << map.height << " " << map.width << std::endl;
        for (int i = 0; i < map.height - 1; ++i) {
            for (int j = 0; j < map.width - 1; ++j) {
                for (int k = 0; k < k_neighbours; ++k) {
                    int i_ = i + neighbours[k][0];
                    int j_ = j + neighbours[k][1];

                    assert(map.height_map[i_][j_] <= 1.0);

                    _points.push_back(glm::vec3(float(i_)/(map.height - 1), float(j_)/(map.width - 1),
                            map.height_map[i_][j_]));
                    _normals.push_back(get_normal(map.height_map, map.height, map.width, i_, j_));
                }
            }
        }
    }

    void makeScene()
    {
        float scale = 20;

        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();
        _surface = generate_mesh();

        glm::tmat4x4<double> translationMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(-scale / 2, -scale / 2, 0.0f));
        glm::vec3 scalingVector = glm::vec3(1.f * scale, 1.f * scale, 0.5);
        glm::tmat4x4<double> scalingMatrix = glm::scale(scalingVector);
        glm::tmat4x4<double> modelMatrix = translationMatrix * scalingMatrix;

        _surface->setModelMatrix(modelMatrix);

        _shader = std::make_shared<ShaderProgram>("592LitvintsevaData1/shader.vert", "592LitvintsevaData1/shader.frag");
    }

    void draw()
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
        _surface->draw();
    }

private:
    glm::vec3 get_normal(const std::vector<std::vector<float>>& map, int height, int width, int i, int j) {
        int left = j, right = j, bottom = i, top = i;

        if (i > 0) {
            top = i - 1;
        }

        if (i < height - 1) {
            bottom = i + 1;
        }

        if (j > 0) {
            left = j - 1;
        }

        if (j < width - 1) {
            right = j + 1;
        }

        glm::vec3 horizonal = glm::vec3(2, map[i][right] - map[i][left], 0);
        glm::vec3 vertical = glm::vec3(0, map[top][j] - map[bottom][j], 2);
        glm::vec3 normal = glm::normalize(glm::cross(vertical, horizonal));

        return normal;
    }

    MeshPtr generate_mesh() {
        DataBufferPtr buf_points = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf_points->setData(_points.size() * sizeof(float) * 3, _points.data());

        DataBufferPtr buf_norms = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf_norms->setData(_normals.size() * sizeof(float) * 3, _normals.data());

        auto surface = std::make_shared<Mesh>();
        surface->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf_points);
        surface->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf_norms);

        surface->setPrimitiveType(GL_TRIANGLES);
        surface->setVertexCount(_points.size());

        surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        return surface;
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
