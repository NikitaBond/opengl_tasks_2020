#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <LightInfo.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <stdlib.h>

#include "MapLoader.h"
#include "PlayerCamera.h"

class SampleApplication : public Application
{
public:
    MapLoader map;
    MeshPtr _surface;
    ShaderProgramPtr _shader;
    std::vector<glm::vec3> _points;
    std::vector<glm::vec3> _normals;
    std::vector<glm::vec2> _texts;

    LightInfo _light;
    float _phi = 1.42;
    float _theta = glm::pi<float>() * 0.2f;

    TexturePtr _snow_texture;
    TexturePtr _sand_texture;
    TexturePtr _stone_texture;
    TexturePtr _grass_texture;

    GLuint _sampler;

    SampleApplication() {
        const int k_neighbours = 6;

        uint8_t neighbours[k_neighbours][2] = {
                {0, 0},
                {0, 1},
                {1, 0},
                {0, 1},
                {1, 0},
                {1, 1},
        };

        map.ReadMap();

        std::cerr << map.height << " " << map.width << std::endl;
        for (int i = 0; i < map.height - 1; ++i) {
            for (int j = 0; j < map.width - 1; ++j) {
                for (int k = 0; k < k_neighbours; ++k) {
                    int i_ = i + neighbours[k][0];
                    int j_ = j + neighbours[k][1];

                    assert(map.height_map[i_][j_] <= 1.0);

                    _points.push_back(glm::vec3(float(i_)/(map.height - 1), float(j_)/(map.width - 1),
                            map.height_map[i_][j_]));
                    _normals.push_back(get_normal(map.height_map, map.height, map.width, i_, j_));
                    _texts.push_back(glm::vec2(float(i_)/20, float(j_)/20));
                }
            }
        }
    }

    void makeScene()
    {
        float scale = 20;

        Application::makeScene();

        _cameraMover = std::make_shared<PlayerCamera>(map, scale);

        _surface = generate_mesh();

        glm::tmat4x4<double> translationMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(-scale / 2, -scale / 2, 0.0f));
        glm::vec3 scalingVector = glm::vec3(1.f * scale, 1.f * scale, 0.5);
        glm::tmat4x4<double> scalingMatrix = glm::scale(scalingVector);
        glm::tmat4x4<double> modelMatrix = translationMatrix * scalingMatrix;

        _surface->setModelMatrix(modelMatrix);

        _shader = std::make_shared<ShaderProgram>("592LitvintsevaData2/shader.vert", "592LitvintsevaData2/shader.frag");

        _snow_texture = loadTexture("592LitvintsevaData2/snow.jpg");
        _sand_texture = loadTexture("592LitvintsevaData2/sand.jpg");
        _stone_texture = loadTexture("592LitvintsevaData2/rock.jpg");
        _grass_texture = loadTexture("592LitvintsevaData2/grass.jpg");


        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glSamplerParameterf(_sampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, static_cast<GLfloat>(4));
    }

    void draw()
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        glm::vec3 lightDir = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));

        _shader->setVec3Uniform("light.dir", lightDir);
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        GLuint textureUnitSand = 0;
        GLuint textureUnitGrass = 1;
        GLuint textureUnitStone = 2;
        GLuint textureUnitSnow = 3;

        bind(_sand_texture, textureUnitSand, "sandTex");
        bind(_grass_texture, textureUnitGrass, "grassTex");
        bind(_stone_texture, textureUnitStone, "stoneTex");
        bind(_snow_texture, textureUnitSnow, "snowTex");

        {
            _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix",
                                    glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _surface->modelMatrix()))));
            _surface->draw();
        }

        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
        }
        ImGui::End();
    }

private:

    void bind(TexturePtr texture, GLuint index, std::string name) {

        glActiveTexture(GL_TEXTURE0 + index); //текстурный юнит
        texture->bind();
        glBindSampler(index, _sampler);

        _shader->setIntUniform(name, index);
    }

    glm::vec3 get_normal(const std::vector<std::vector<float>>& map, int height, int width, int i, int j) {
        int left = j, right = j, bottom = i, top = i;

        if (i > 0) {
            top = i - 1;
        }

        if (i < height - 1) {
            bottom = i + 1;
        }

        if (j > 0) {
            left = j - 1;
        }

        if (j < width - 1) {
            right = j + 1;
        }

        glm::vec3 horizonal = glm::vec3(2, map[i][right] - map[i][left], 0);
        glm::vec3 vertical = glm::vec3(0, map[top][j] - map[bottom][j], 2);
        glm::vec3 normal = glm::normalize(glm::cross(vertical, horizonal));

        return normal;
    }

    MeshPtr generate_mesh() {
        DataBufferPtr buf_points = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf_points->setData(_points.size() * sizeof(float) * 3, _points.data());

        DataBufferPtr buf_norms = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf_norms->setData(_normals.size() * sizeof(float) * 3, _normals.data());

        DataBufferPtr buf_textures = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf_textures->setData(_texts.size() * sizeof(float) * 2, _texts.data());

        auto surface = std::make_shared<Mesh>();
        surface->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf_points);
        surface->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf_norms);
        surface->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf_textures);

        surface->setPrimitiveType(GL_TRIANGLES);
        surface->setVertexCount(_points.size());

        surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        return surface;
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
